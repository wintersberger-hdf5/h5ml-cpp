//
// (c) Copyright 2014 DESY, Eugen Wintersberger <eugen.wintersberger@desy.de>
//
// This file is part of libpnicore.
//
// libpnicore is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libpnicore is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libpnicore.  If not, see <http://www.gnu.org/licenses/>.
//
// ===========================================================================
//
//  Created on: Apr 11, 2014
//      Author: Eugen Wintersberger <eugen.wintersberger@desy.de>
//
#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <h5ml++/types/container_trait.hpp>

using namespace hdf5::ml::types;

TEST_CASE("testing the container trait") { 

    //=========================================================================
    SECTION("test vectors")
    {
        using vector_type =  std::vector<size_t>;
        using trait_type = container_trait<vector_type>;

        REQUIRE(trait_type::is_random_access);
        REQUIRE(trait_type::is_iterable);
        REQUIRE(trait_type::is_contiguous);
        REQUIRE(!trait_type::is_multidim);
    }

    //=========================================================================
    SECTION("test array")
    {
        using array_type = std::array<size_t,4>;
        using trait_type = container_trait<array_type>;

        REQUIRE(trait_type::is_random_access);
        REQUIRE(trait_type::is_iterable);
        REQUIRE(trait_type::is_contiguous);
        REQUIRE_FALSE(trait_type::is_multidim);
    }

    //=========================================================================
    SECTION("test lists")
    {
        using list_type = std::list<size_t>;
        using trait_type = container_trait<list_type>;

        REQUIRE_FALSE(trait_type::is_random_access);
        REQUIRE(trait_type::is_iterable);
        REQUIRE_FALSE(trait_type::is_contiguous);
        REQUIRE_FALSE(trait_type::is_multidim);
    }

}