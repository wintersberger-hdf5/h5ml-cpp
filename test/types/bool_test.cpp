//
// (c) Copyright 2014 DESY, Eugen Wintersberger <eugen.wintersberger@desy.de>
//
// This file is part of libpnicore.
//
// libpnicore is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libpnicore is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libpnicore.  If not, see <http://www.gnu.org/licenses/>.
//
// ============================================================================
//
//  Created on: Apr 11, 2014
//      Author: Eugen Wintersberger <eugen.wintersberger@desy.de>
//
#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <iostream>
#include <h5ml++/types/bool.hpp>

using namespace hdf5::ml::types;


TEST_CASE("testing the bool type","[hdf5],[ml]") { 

    //========================================================================
    SECTION("test construction")
    {
        bool_t bvalue;  //default constructed - should be false
        REQUIRE(!bvalue);
        
        bool_t bvalue2 = true; 
        REQUIRE(bvalue2);
    }

    //========================================================================
    SECTION("test comparison")
    {
        bool_t v1;
        bool_t v2 = true;
        bool_t v3 = false;

        REQUIRE(v1 != v2);
        REQUIRE(v1 == v3);
    }

    //========================================================================
    SECTION("test assignment")
    {
        bool_t v1;
        REQUIRE_FALSE(v1);

        v1 = true;
        REQUIRE(v1);
    }

    //========================================================================
    SECTION("test compatibility")
    {
        using vector_type = std::vector<bool_t>;
        vector_type v(10);
        
        bool_t *ptr = v.data();
        for(auto x: v) REQUIRE(x == *ptr++);
    }

    //========================================================================
    SECTION("test operators")
    {
        bool_t v1 = true;
        bool_t v2 = false;

        REQUIRE_FALSE((v1 && v2));
        REQUIRE((v1 && v1));

        REQUIRE((v1 || v2));
        REQUIRE_FALSE((v2 || v2));
    }

}
