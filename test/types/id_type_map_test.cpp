//
// (c) Copyright 2015 DESY, Eugen Wintersberger <eugen.wintersberger@desy.de>
//
// This file is part of libpnicore.
//
// libpnicore is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libpnicore is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libpnicore.  If not, see <http://www.gnu.org/licenses/>.
//
// ===========================================================================
//
//  Created on: Oct 15, 2015
//      Author: Eugen Wintersberger <eugen.wintersberger@desy.de>
//
#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <h5ml++/types/id_type_map.hpp>
#include <iostream>

using namespace hdf5::ml::types;

TEST_CASE("testing the id_type_map","[hdf5]") {

    //========================================================================
    SECTION("test for an 8 Bit unsigned integer")
    {
        using  map_type = id_type_map<type_id_t::UINT8>;
        REQUIRE(std::is_same<map_type::type,std::uint8_t>::value);
    }

    //========================================================================
    SECTION("test for 8Bit signed integer")
    {
        using map_type = id_type_map<type_id_t::INT8>;
        REQUIRE(std::is_same<map_type::type,std::int8_t>::value);
    }

    //========================================================================
    SECTION("test for a 16 Bit unsigned integer")
    {
        using map_type = id_type_map<type_id_t::UINT16>;
        REQUIRE(std::is_same<map_type::type,std::uint16_t>::value);
    }

    //========================================================================
    SECTION("test for a 16 Bit signed integer")
    {
        using map_type = id_type_map<type_id_t::INT16>;
        REQUIRE(std::is_same<map_type::type,std::int16_t>::value);
    }

    //========================================================================
    SECTION("test for a 32 Bit unsigned integer")
    {
        using map_type = id_type_map<type_id_t::UINT32>;
        REQUIRE(std::is_same<map_type::type,std::uint32_t>::value);
    }

    //========================================================================
    SECTION("test for a 32 bit singed integer")
    {
        using map_type = id_type_map<type_id_t::INT32>;
        REQUIRE(std::is_same<map_type::type,std::int32_t>::value);
    }

    //========================================================================
    SECTION("test for a 64 bit unsigned integer")
    {
        using map_type =id_type_map<type_id_t::UINT64>;
        REQUIRE(std::is_same<map_type::type,std::uint64_t>::value);
    }

    //========================================================================
    SECTION("test for a 64 bit signed integer")
    {
        using map_type = id_type_map<type_id_t::INT64>;
        REQUIRE(std::is_same<map_type::type,std::int64_t>::value);
    }

    //========================================================================
    SECTION("test for a 32 bit floating point number")
    {
        using map_type = id_type_map<type_id_t::FLOAT32>;
        REQUIRE(std::is_same<map_type::type,float>::value);
    }

    //========================================================================
    SECTION("test for a 64 bit floating point number")
    {
        using map_type = id_type_map<type_id_t::FLOAT64>;
        REQUIRE(std::is_same<map_type::type,double>::value);
    }

    //========================================================================
    SECTION("test for 128 bit floating point number")
    {
        using map_type = id_type_map<type_id_t::FLOAT128>;
        REQUIRE(std::is_same<map_type::type,long double>::value);
    }

    //========================================================================
    SECTION("testing for complex<float>")
    {
        using map_type = id_type_map<type_id_t::COMPLEX32>;
        REQUIRE(std::is_same<map_type::type,std::complex<float>>::value);
    }

    //========================================================================
    SECTION("testing for complex<double>")
    {
        using map_type = id_type_map<type_id_t::COMPLEX64>;
        REQUIRE(std::is_same<map_type::type,std::complex<double>>::value);
    }

    //========================================================================
    SECTION("testing for complex<long double>")
    {
        using map_type = id_type_map<type_id_t::COMPLEX128>;
        REQUIRE(std::is_same<map_type::type,std::complex<long double>>::value);
    }
    
    //========================================================================
    SECTION("testing for std::string")
    {
        using map_type = id_type_map<type_id_t::STRING>;
        REQUIRE((std::is_same<map_type::type,std::string>::value));
    }

    //========================================================================
    SECTION("testing for binary type")
    {
        using map_type = id_type_map<type_id_t::BINARY>;
        REQUIRE((std::is_same<map_type::type,binary>::value));
    }

    //========================================================================
    SECTION("testing for bool")
    {
        using map_type = id_type_map<type_id_t::BOOL>;
        REQUIRE((std::is_same<map_type::type,bool_t>::value));
    }

    //========================================================================
    SECTION("testing for none")
    {
        using map_type = id_type_map<type_id_t::NONE>;
        REQUIRE((std::is_same<map_type::type,none>::value));
    }
}