//
// (c) Copyright 2015 DESY, Eugen Wintersberger <eugen.wintersberger@desy.de>
//
// This file is part of libpnicore.
//
// libpnicore is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libpnicore is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libpnicore.  If not, see <http://www.gnu.org/licenses/>.
//
// ===========================================================================
//
//  Created on: Oct 15, 2015
//      Author: Eugen Wintersberger <eugen.wintersberger@desy.de>
//
#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <h5ml++/types/type_utils.hpp>
#include <tuple>
#include <string>

using namespace hdf5::ml::types;

TEST_CASE("test string representation of a type_id_t","[hdf5]") {
    using test_t = std::tuple<type_id_t,std::string, std::string>;
    auto data = GENERATE(table<type_id_t, std::string,std::string>({
        test_t{type_id_t::INT8,"int8","i8"},
        test_t{type_id_t::UINT8,"uint8","ui8"},
        test_t{type_id_t::INT16,"int16","i16"},
        test_t{type_id_t::UINT16,"uint16","ui16"},
        test_t{type_id_t::INT32,"int32","i32"},
        test_t{type_id_t::UINT32,"uint32","ui32"},
        test_t{type_id_t::INT64,"int64","i64"},
        test_t{type_id_t::UINT64,"uint64","ui64"},
        test_t{type_id_t::FLOAT32,"float32","f32"},
        test_t{type_id_t::FLOAT64,"float64","f64"},
        test_t{type_id_t::FLOAT128,"float128","f128"},
        test_t{type_id_t::COMPLEX32,"complex32","c32"},
        test_t{type_id_t::COMPLEX64,"complex64","c64"},
        test_t{type_id_t::COMPLEX128,"complex128","c128"},
        test_t{type_id_t::STRING,"string","str"},
        test_t{type_id_t::BINARY,"binary","binary"},
        test_t{type_id_t::BOOL,"bool","bool"},
        test_t{type_id_t::NONE,"none","none"}}));
    SECTION("test type_id_t to string conversion")
    {
        REQUIRE(str_from_type_id(std::get<0>(data)) == std::get<1>(data));
    }

    SECTION("test type_id_t from string constrution") {
        auto id = std::get<0>(data);
        REQUIRE(type_id_from_str(std::get<1>(data)) == id);
        REQUIRE(type_id_from_str(std::get<2>(data)) == id);
    }
}
