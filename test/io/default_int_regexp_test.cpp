//
// (c) Copyright 2017 DESY
//
// This file is part of libpniio.
//
// libpniio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libpniio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libpniio.  If not, see <http://www.gnu.org/licenses/>.
// ===========================================================================
//
// Created on: Jun 7, 2017
//     Author: Eugen Wintersberger <eugen.wintersberger@desy.de>
//
//
#include <catch.hpp>
#include <h5ml++/io/parsers/parser.hpp>

using namespace hdf5::ml::io;

SCENARIO("the default regular expression for integer numbers")
{
    GIVEN("+123")
    {
        THEN("the regular expression for integers must match")
        {
            REQUIRE(boost::regex_match("+123", default_int_regexp))
        }
    }
    WHEN("-3243")
    {
        THEN("the regular expression for integers must match")
        {
            REQUIRE(boost::regex_match("-3243", default_int_regexp));
        }
    }
    GIVEN("124")
    {
        THEN("the regular expression for integers must match")
        {

            REQUIRE(boost::regex_match("124", default_int_regexp));
        }
    }
    GIVEN("2345562")
    {
        THEN("the default integer regular expresion must match")
        {
            REQUIRE(boost::regex_match("2345562", default_int_regexp));
        }
    }
    GIVEN("12343.")
    {
        THEN("the regular expression for integers must not match")
        {
            REQUIRE_FALSE(!boost::regex_match("12343.", default_int_regexp));
        }
    }

    GIVEN("-1234.23")
    {
        THEN("the regular expression for integers must not match")
        {
            REQUIRE_FALSE(!boost::regex_match("-1234.23", default_int_regexp));
        }
    }

    GIVEN("+-1234")
    {
        THEN("the regular expression for integers must not match")
        {

            REQUIRE_FALSE(!boost::regex_match("+-1234", default_int_regexp));
        }
    }
}
