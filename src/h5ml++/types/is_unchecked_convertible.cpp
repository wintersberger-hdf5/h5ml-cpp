//
// (c) Copyright 2014 DESY, Eugen Wintersberger <eugen.wintersberger@desy.de>
//
// This file is part of libpniio.
//
// libpniio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libpniio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libpniio.  If not, see <http://www.gnu.org/licenses/>.
// 
// ===========================================================================
//
//  Created on: Dec 19, 2014
//      Author: Eugen Wintersberger <eugen.wintersberger@desy.de>
//

#include <h5ml++/types/type_conversion.hpp>
#include <h5ml++/types/unchecked_convertible.hpp>

#include <map>

#include "utils.hpp"

namespace hdf5{
namespace ml{
namespace types {

    

    static const std::map<type_id_t,type_id_vector> conversion_map{
        generate_map_element<std::uint8_t,unchecked_type_vectors>(),
        generate_map_element<std::uint16_t,unchecked_type_vectors>(),
        generate_map_element<std::uint32_t,unchecked_type_vectors>(),
        generate_map_element<std::uint64_t,unchecked_type_vectors>(),
        generate_map_element<std::int8_t,unchecked_type_vectors>(),
        generate_map_element<std::int16_t,unchecked_type_vectors>(),
        generate_map_element<std::int32_t,unchecked_type_vectors>(),
        generate_map_element<std::int64_t,unchecked_type_vectors>(),
        generate_map_element<float,unchecked_type_vectors>(),
        generate_map_element<double,unchecked_type_vectors>(),
        generate_map_element<long double,unchecked_type_vectors>(),
        generate_map_element<std::complex<float>,unchecked_type_vectors>(),
        generate_map_element<std::complex<double>,unchecked_type_vectors>(),
        generate_map_element<std::complex<long double>,unchecked_type_vectors>()
    };


    //------------------------------------------------------------------------
    bool is_unchecked_convertible(type_id_t source_tid,type_id_t target_tid)
    {
        type_id_vector types = conversion_map.at(source_tid);
        
        return std::count(types.begin(),types.end(),target_tid)!=0;
    }


//end of namespace
}
}
}

