//
// (c) Copyright 2011 DESY, Eugen Wintersberger <eugen.wintersberger@desy.de>
//
// This file is part of libpnicore.
//
// libpnicore is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libpnicore is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libpnicore.  If not, see <http://www.gnu.org/licenses/>.
//
// ============================================================================
//
// Created on: Oct 13, 2011
//     Author: Eugen Wintersberger
//
//
#pragma once

#include <h5ml++/types/types.hpp>

namespace hdf5{
namespace ml{
namespace types {

//!
//! \ingroup type_classes_internal
//! \brief macro for ID to type map specialization
//! 
//! This macro is used to create a specialization of IDTypeMap.
//!
#define CREATE_ID_TYPE_MAP(tid,dtype)\
    template<> struct id_type_map<tid>\
    {\
        using type =dtype;\
    }

    //-------------------------------------------------------------------------
    //! 
    //! \ingroup type_classes
    //! \brief map from type_id_t to type
    //!
    //! This template implements a map from a type_id_t to a specific data 
    //! type. The type map can be used in a template to determine the type of a 
    //! variable by the ID rather than by its type:
    /*!
    \code
    id_type_map<ID>::type variable;
    \endcode
    !*/ 
    //! \tparam id value of the type id
    //!
    template<type_id_t id> 
    struct id_type_map
    {
        //! type identified by the template parameter
        using type = std::uint8_t; 
    };

    //! \cond NO_API_DOC
    CREATE_ID_TYPE_MAP(type_id_t::UINT8,std::uint8_t);
    CREATE_ID_TYPE_MAP(type_id_t::INT8,std::int8_t);
    CREATE_ID_TYPE_MAP(type_id_t::UINT16,std::uint16_t);
    CREATE_ID_TYPE_MAP(type_id_t::INT16,std::int16_t);
    CREATE_ID_TYPE_MAP(type_id_t::UINT32,std::uint32_t);
    CREATE_ID_TYPE_MAP(type_id_t::INT32,std::int32_t);
    CREATE_ID_TYPE_MAP(type_id_t::UINT64,std::uint64_t);
    CREATE_ID_TYPE_MAP(type_id_t::INT64,std::int64_t);
    CREATE_ID_TYPE_MAP(type_id_t::FLOAT32,float);
    CREATE_ID_TYPE_MAP(type_id_t::FLOAT64,double);
    CREATE_ID_TYPE_MAP(type_id_t::FLOAT128,long double);
    CREATE_ID_TYPE_MAP(type_id_t::COMPLEX32,std::complex<float>);
    CREATE_ID_TYPE_MAP(type_id_t::COMPLEX64,std::complex<double>);
    CREATE_ID_TYPE_MAP(type_id_t::COMPLEX128,std::complex<long double>);
    CREATE_ID_TYPE_MAP(type_id_t::STRING,std::string);
    CREATE_ID_TYPE_MAP(type_id_t::BINARY,binary);
    CREATE_ID_TYPE_MAP(type_id_t::BOOL,bool_t);
    CREATE_ID_TYPE_MAP(type_id_t::NONE,none);
    //! \endcond NO_API_DOC

} // end of namespace types
} // end of namespace ml
} // end of namespace hdf5