//
// (c) Copyright 2012 DESY, Eugen Wintersberger <eugen.wintersberger@desy.de>
//
// This file is part of libpnicore.
//
// libpnicore is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libpnicore is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libpnicore.  If not, see <http://www.gnu.org/licenses/>.
//
// ============================================================================
//
//  Created on: Sep 27, 2012
//      Author: Eugen Wintersberger
//
#pragma once

#include <h5ml++/types/types.hpp>

namespace hdf5{
namespace ml{
namespace types{
   
//!
//! \ingroup type_classes_internal
//! \brief macro for type/class map creation
//! 
//! This macro creates a type to type class map specialization. 
//!
#define CREATE_TYPE_CLASS_MAP(type,tclass)\
    template<> struct type_class_map<type>\
    {\
        static const type_class_t type_class = tclass;\
    }
    
    //!
    //! \ingroup type_classes
    //! \brief type to class mapping
    //!  
    //! Template mapping a particular data type to its type class.
    //! \tparam T data type
    //!
    template<typename T> 
    struct type_class_map
    {
        //! class of type T
        static const type_class_t type_class = type_class_t::NONE;
    };

    //! \cond NO_API_DOC
    CREATE_TYPE_CLASS_MAP(std::uint8_t,type_class_t::INTEGER);
    CREATE_TYPE_CLASS_MAP(std::int8_t,type_class_t::INTEGER);
    CREATE_TYPE_CLASS_MAP(std::uint16_t,type_class_t::INTEGER);
    CREATE_TYPE_CLASS_MAP(std::int16_t,type_class_t::INTEGER);
    CREATE_TYPE_CLASS_MAP(std::uint32_t,type_class_t::INTEGER);
    CREATE_TYPE_CLASS_MAP(std::int32_t,type_class_t::INTEGER);
    CREATE_TYPE_CLASS_MAP(std::uint64_t,type_class_t::INTEGER);
    CREATE_TYPE_CLASS_MAP(std::int64_t,type_class_t::INTEGER);
    
    CREATE_TYPE_CLASS_MAP(float,type_class_t::FLOAT);
    CREATE_TYPE_CLASS_MAP(double,type_class_t::FLOAT);
    CREATE_TYPE_CLASS_MAP(long double,type_class_t::FLOAT);

    CREATE_TYPE_CLASS_MAP(std::complex<float>,type_class_t::COMPLEX);
    CREATE_TYPE_CLASS_MAP(std::complex<double>,type_class_t::COMPLEX);
    CREATE_TYPE_CLASS_MAP(std::complex<long double>,type_class_t::COMPLEX);

    CREATE_TYPE_CLASS_MAP(bool_t,type_class_t::BOOL);
    CREATE_TYPE_CLASS_MAP(binary,type_class_t::BINARY);
    CREATE_TYPE_CLASS_MAP(std::string,type_class_t::STRING);

    CREATE_TYPE_CLASS_MAP(none,type_class_t::NONE);

    //! \endcond NO_API_DOC

//end of namespace
} // end of namespace types
} // end of namespace ml
} // end of namespace hdf5
