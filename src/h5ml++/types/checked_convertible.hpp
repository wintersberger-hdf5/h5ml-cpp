//
// (c) Copyright 2014 DESY, Eugen Wintersberger <eugen.wintersberger@desy.de>
//
// This file is part of libpnicore.
//
// libpnicore is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libpnicore is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libpnicore.  If not, see <http://www.gnu.org/licenses/>.
//
// ============================================================================
//
//  Created on: Dec 18, 2014
//      Author: Eugen Wintersberger
//
#pragma once

#include <boost/mpl/pair.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/contains.hpp>
#include <boost/mpl/map.hpp>
#include <boost/mpl/at.hpp>
#include <complex>
#include <cstdint>
#include <h5ml++/types/binary.hpp>
#include <h5ml++/types/bool.hpp>

namespace hdf5 {
namespace ml {
namespace types {

      //!
      //! \ingroup type_classes_internal
      //! \brief unchecked convertible map
      //!
      //! This map provides for each type a list of types to which this type
      //! can be converted only with range checking.
      using checked_type_vectors = boost::mpl::map<
          //-------------source type uint8--------------------------------------
          boost::mpl::pair<std::uint8_t, boost::mpl::vector<std::int8_t>>,

          //----------------source type uint16----------------------------------
          boost::mpl::pair<
              std::uint16_t,
              boost::mpl::vector<std::uint8_t, std::int8_t, std::int16_t>>,

          //----------------------source type uint32----------------------------
          boost::mpl::pair<
              std::uint32_t,
              boost::mpl::vector<std::uint8_t, std::uint16_t,
                                 std::int8_t, std::int16_t, std::int32_t>>,

          //--------------------------source type uint64------------------------
          boost::mpl::pair<
              std::uint64_t,
              boost::mpl::vector<std::uint8_t, std::uint16_t, std::uint32_t,
                                 std::int8_t, std::int16_t, std::int32_t, std::int64_t>>,

          //------------------------source type int8 ---------------------------
          boost::mpl::pair<
              std::int8_t,
              boost::mpl::vector<std::uint8_t, std::uint16_t, std::uint32_t, std::uint64_t>>,

          //-----------------------source type int16----------------------------
          boost::mpl::pair<
              std::int16_t,
              boost::mpl::vector<std::uint8_t, std::uint16_t, std::uint32_t, std::uint64_t, std::int8_t>>,

          //------------------------source type int32---------------------------
          boost::mpl::pair<
              std::int32_t,
              boost::mpl::vector<std::uint8_t, std::uint16_t, std::uint32_t, std::uint64_t, std::int8_t, std::int16_t>>,

          //------------------------source type int64----------------------------
          boost::mpl::pair<
              std::int64_t,
              boost::mpl::vector<std::uint8_t, std::uint16_t, std::uint32_t, std::uint64_t,
                                 std::int8_t, std::int16_t, std::int32_t>>,

          //-------------------------source type float64------------------------
          boost::mpl::pair<float, boost::mpl::vector<>>,
          boost::mpl::pair<double, boost::mpl::vector<float, std::complex<float>>>,

          //-------------------source type float128-----------------------------
          boost::mpl::pair<long double, boost::mpl::vector<float, double,
                                                           std::complex<float>, std::complex<double>>>,

          //-------------------source type complex64----------------------------
          boost::mpl::pair<std::complex<float>, boost::mpl::vector<>>,
          boost::mpl::pair<
              std::complex<double>,
              boost::mpl::vector<std::complex<float>>>,

          //-------------------source type complex128---------------------------
          boost::mpl::pair<std::complex<long double>, boost::mpl::vector<std::complex<float>, std::complex<double>>>,

          boost::mpl::pair<std::string, boost::mpl::vector<>>,
          boost::mpl::pair<binary, boost::mpl::vector<>>,
          boost::mpl::pair<bool_t, boost::mpl::vector<>>

          >;

      //------------------------------------------------------------------------
      //!
      //! \ingroup type_classes
      //! \brief check if a type is unchecked convertible
      //!
      //! This template provides information about if a type can be converted
      //! to another without having the range checked.
      //!
      //! \tparam ST source type
      //! \tparam TT target type
      template <
          typename ST,
          typename TT>
      struct checked_convertible
      {
        //! select the type vector
        typedef typename boost::mpl::at<checked_type_vectors, ST>::type map_element;
        //! type to check if TT is in the map
        typedef boost::mpl::contains<map_element, TT> c;
        //! result of the search
        static const bool value = c::value;
      };

      //end of namespace
    }
  }
}