//
// (c) Copyright 2017 DESY
//
// This file is part of libpniio.
//
// libpniio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libpniio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libpniio.  If not, see <http://www.gnu.org/licenses/>.
// ===========================================================================
//
// Author: Eugen Wintersberger <eugen.wintersberger@desy.de>
// Created on: Dec 13, 2017
//
#include <h5ml++/xml/v1/data_writer.hpp>
#include <h5ml++/io/parsers.hpp>

namespace hdf5 {
namespace ml {
namespace xml {
namespace v1 {

template<typename T,typename OBJ>
void write_data(const std::string &data,const OBJ &object)
{
  hdf5::ml::io::parser<std::vector<T>> parser;
  object.write(parser(data));
}

template<typename OBJ>
void write_string_data(const std::string &data,const OBJ &object)
{
  object.write(data);
}

template<typename OBJ>
void write_data(const Node &node,const OBJ &object)
{
  using namespace hdf5::ml::types;
  type_id_t type_id = type_id_from_str(node.attribute("type").str_data());
  std::string data = node.str_data();

  if(data.empty()) return;

  switch(type_id)
  {
    case type_id_t::UINT8: write_data<std::uint8_t>(data,object); break;
    case type_id_t::INT8:  write_data<std::int8_t>(data,object);  break;
    case type_id_t::UINT16: write_data<std::uint16_t>(data,object); break;
    case type_id_t::INT16: write_data<std::int16_t>(data,object);   break;
    case type_id_t::UINT32: write_data<std::uint32_t>(data,object); break;
    case type_id_t::INT32:  write_data<std::int32_t>(data,object);  break;
    case type_id_t::UINT64: write_data<std::uint64_t>(data,object); break;
    case type_id_t::INT64:  write_data<std::int64_t>(data,object);  break;
    case type_id_t::FLOAT32: write_data<float>(data,object); break;
    case type_id_t::FLOAT64: write_data<double>(data,object); break;
    case type_id_t::FLOAT128: write_data<long double>(data,object); break;
    case type_id_t::STRING:   write_string_data(data,object); break;
    default:
    {
      std::stringstream ss;
      ss<<"Unsupported datat type: "<<type_id;
      throw std::runtime_error(ss.str());
    }
  }
}



DataWriter::DataWriter(const Node &node):
    node_(node)
{}

void DataWriter::write(const hdf5::node::Dataset &dataset) const
{
  write_data(node_,dataset);
}

void DataWriter::write(const hdf5::attribute::Attribute &attribute) const
{
  write_data(node_,attribute);
}

} // namespace v1
} // namespace xml
} // namespace ml
} // namespace hdf5
