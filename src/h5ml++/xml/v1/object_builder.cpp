//
// (c) Copyright 2017 DESY
//
// This file is part of libpniio.
//
// libpniio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libpniio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libpniio.  If not, see <http://www.gnu.org/licenses/>.
// ===========================================================================
//
// Author: Eugen Wintersberger <eugen.wintersberger@desy.de>
// Created on: Dec 8, 2017
//

#include <h5ml++/xml/v1/object_builder.hpp>
#include <h5ml++/xml/v1/builder_factory.hpp>

namespace hdf5 {
namespace ml {
namespace xml {
namespace v1 {

ObjectBuilder::~ObjectBuilder()
{}

ObjectBuilder::ObjectBuilder(const Node &xml_node):
    xml_node_(xml_node)
{}

const Node &ObjectBuilder::node() const noexcept
{
  return xml_node_;
}

void ObjectBuilder::build(const hdf5::node::Node &parent) const
{
  for(auto element: xml_node_)
  {
    UniquePointer builder = BuilderFactory::create(element);

    if(builder) builder->build(parent);
  }
}





} // namespace v1
} // namespace xml
} // namespace ml
} // namespace hdf5
