//
// (c) Copyright 2017 DESY
//
// This file is part of libpniio.
//
// libpniio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libpniio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libpniio.  If not, see <http://www.gnu.org/licenses/>.
// ===========================================================================
//
// Author: Eugen Wintersberger <eugen.wintersberger@desy.de>
// Created on: Dec 8, 2017
//
#include <h5ml++/xml/v1/object_builder.hpp>
#include <h5ml++/xml/v1/group_builder.hpp>
#include <h5ml++/xml/v1/field_builder.hpp>
#include <h5ml++/xml/v1/attribute_builder.hpp>
#include <h5ml++/xml/v1/link_builder.hpp>

namespace hdf5 {
namespace ml {
namespace xml {
namespace v1 {

ObjectBuilder::UniquePointer BuilderFactory::create(const Node::value_type &element)
{
  if(element.first == "field")
    return std::make_unique<FieldBuilder>(element.second);
  else if(element.first == "group")
    return std::make_unique<GroupBuilder>(element.second);
  else if(element.first == "attribute")
    return std::make_unique<AttributeBuilder>(element.second);
  else if(element.first == "link")
    return std::make_unique<LinkBuilder>(element.second);
  else
  {
    return nullptr;
  }
}


} // namespace xml
} // namespace nexus
} // namespace io
} // namespace pni
