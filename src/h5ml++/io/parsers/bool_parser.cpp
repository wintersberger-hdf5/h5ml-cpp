//
// (c) Copyright 2017 DESY
//
// This file is part of libpniio.
//
// libpniio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libpniio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libpniio.  If not, see <http://www.gnu.org/licenses/>.
// ===========================================================================
//
// Created on: Jun 7, 2017
//     Author: Eugen Wintersberger <eugen.wintersberger@desy.de>
//
//

#include <h5ml++/io/parsers/bool_parser.hpp>
#include <stdexcept>

namespace hdf5{
namespace ml {
namespace io {

    parser<hdf5::ml::types::bool_t>::parser():
        _true_regex("^T(rue|RUE)|true|1$"),
        _false_regex("^F(alse|ALSE)|false|0$")
    {}


    parser<hdf5::ml::types::bool_t>::parser(const std::string &true_regex,
                         const std::string &false_regex):
        _true_regex(true_regex),
        _false_regex(false_regex)
    {}

    parser<hdf5::ml::types::bool_t>::result_type
    parser<hdf5::ml::types::bool_t>::operator()(const std::string &input) const
    {
        if(boost::regex_match(input,_true_regex))
            return true;
        else if(boost::regex_match(input,_false_regex))
            return false;
        else
        {
            std::stringstream ss;
            ss<<"Input ["<<input<<"] cannot be converted to a boolean value!";
            throw std::domain_error(ss.str());
        }
    }

} // end of namespace io
} // end of namespace ml
} // end of namespace hdf5
