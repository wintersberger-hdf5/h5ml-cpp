//
// (c) Copyright 2016 DESY
//               2021 Eugen Wintersberger <eugen.wintersberger@gmail.com>
//
// This file is part of libpniio.
//
// h5ml++ is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// h5ml++ is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h5ml++.  If not, see <http://www.gnu.org/licenses/>.
//
// ===========================================================================
//
// Created on: Feb 12, 2016
//     Author: Eugen Wintersberger <eugen.wintersberger@desy.de>
//
#pragma once

#ifdef _MSC_VER
	#ifdef DLL_BUILD
		#define PNIIO_EXPORT __declspec(dllexport)
	#else
		#define PNIIO_EXPORT __declspec(dllimport)
	#endif
#else 
	#define H5ML_EXPORT
#endif

